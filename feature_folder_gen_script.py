import os
import sys

def create_structure(base, structure):
    """
    Recursively creates directories and files based on the provided structure.
    """
    if isinstance(structure, dict):
        for folder, sub_structure in structure.items():
            path = os.path.join(base, folder)
            os.makedirs(path, exist_ok=True)
            create_structure(path, sub_structure)
    elif isinstance(structure, list):
        for file_name in structure:
            file_path = os.path.join(base, file_name)
            with open(file_path, 'w') as f:
                f.write('// This is the start of ' + file_name)

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: python create_flutter_project.py <feature_name>")
        sys.exit(1)

    feature_name = sys.argv[1]

    # Define the structure with the dynamic project name
    structure = {
        f'{feature_name}': {
            'data': [
                f"{feature_name}_datasource.dart",
                f"{feature_name}_repository.dart"
            ],
            "presentation": {
                "pages": [f"{feature_name}_page.dart"],
                "widgets": [],
                "blocs": {
                    f"{feature_name}": [
                        f"{feature_name}_event.dart",
                        f"{feature_name}_state.dart",
                        f"{feature_name}_bloc.dart",
                    ],
                }
            }
        }
    }

    create_structure('./lib/features', structure)
    print(f"Project '{feature_name}' structure has been created successfully.")

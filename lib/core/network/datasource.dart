import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:blist/core/network/dio/dio_service.dart';
import 'package:blist/core/network/dio/exceptions.dart';

/// A base class for app data sources handling network requests.
class BaseDataSource {
  final DioService _dioService;

  BaseDataSource(this._dioService);

  /// Makes a network request based on the specified parameters.
  ///
  /// - [endpoint]: The endpoint of the API to be called.
  /// - [method]: The HTTP request method (GET, POST, etc.).
  /// - [data]: Optional data to be sent with the request (e.g., request body).
  ///
  /// Returns a [Map<String, dynamic>] representing the response data,
  /// or `null` if the request fails.
  ///
  /// Throws a [CustomException] if any errors occur during the request.
  Future<Map<String, dynamic>> makeRequest({
    required String endpoint,
    required RequestMethod method,
    Map<String, dynamic>? data,
    Map<String, dynamic>? queryParams,
    List<MultipartFile>? files,
    Options? options,
  }) async {
    try {
      switch (method) {
        case RequestMethod.get:
          return await _dioService.get(
            endpoint: endpoint,
            queryParams: queryParams,
            options: options,
          );

        case RequestMethod.post:
          return await _dioService.post(
            data: data,
            endpoint: endpoint,
            queryParams: queryParams,
            options: options,
          );

        case RequestMethod.postFormData:
          return await _dioService.postFormData(
            data: data,
            files: files,
            endpoint: endpoint,
            queryParams: queryParams,
            options: options,
          );

        case RequestMethod.put:
          return await _dioService.put(
            data: data,
            endpoint: endpoint,
            options: options,
          );

        case RequestMethod.delete:
          return await _dioService.delete(
            data: data,
            endpoint: endpoint,
            queryParams: queryParams,
            options: options,
          );
      }
    } on Exception catch (e) {
      throw CustomException.fromDioException(e);
    }
  }

  Future<Uint8List> downloadFile({
    required String endpoint,
  }) async {
    try {
      return await _dioService.get(
        endpoint: endpoint,
        options: Options(responseType: ResponseType.bytes),
      );
    } on Exception catch (e) {
      throw CustomException.fromDioException(e);
    }
  }
}

/// Enum representing different HTTP request methods.
enum RequestMethod {
  get,
  post,
  postFormData,
  put,
  delete,
}

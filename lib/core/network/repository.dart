import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:blist/core/network/dio/exceptions.dart';

/// A base class for repository handling data fetching and parsing.
class BaseRepository {
  

  /// Performs data fetching and parsing based on provided functions.
  ///
  /// The [datasource] function is responsible for fetching the raw data.
  ///
  /// The [modelParser] function, if provided, is responsible for parsing the raw data into the desired model.
  ///
  /// Returns the parsed model or null, depending on whether parsing was requested.
  ///
  /// Throws a [CustomException] if any errors occur during parsing.
  Future<dynamic> repository<T>(
    Future Function() datasource, {
    T Function(Map<String, dynamic>)? modelParser,
    bool getModelList = false,
    String? dataKey,
  }) async {
    try {
      final json = await datasource.call();
      final key = dataKey != null ? json[dataKey] : json;
      if (modelParser != null) {
        if (getModelList) {
          return List<T>.from(key.map((x) => modelParser(x)));
        } else {
          return modelParser(key);
        }
      }
      return key;
    } on CustomException {
      rethrow;
    } catch (e, stacktrace) {
      // In Debug, print where the parsing error happened
      debugPrint(e.toString());
      debugPrint(stacktrace.toString());
      throw CustomException.fromParsingException();
    }
  }
}

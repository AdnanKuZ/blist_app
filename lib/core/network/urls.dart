abstract class ApiUrls {
  static String baseUrl = 'https://dummyjson.com';
  static String loginUrl = '$baseUrl/auth/login';
  static String todosUrl = '$baseUrl/todos';
  static String addtodosUrl = '$todosUrl/add';
  static String todoUrl(String id) => '$todosUrl/$id';
}

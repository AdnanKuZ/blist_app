class PaginationModel {
  final int total;
  final int limit;
  final int skip;

  PaginationModel({
    required this.total,
    required this.limit,
    required this.skip,
  });

  int get pageCount => (total / (skip == 0 ? limit : skip)).ceil();

  factory PaginationModel.fromJson(Map<String, dynamic> json) =>
      PaginationModel(
        total: json['total'],
        limit: json['limit'],
        skip: json['skip'],
      );
}

import 'package:audioplayers/audioplayers.dart';

abstract class AudioService {
  static void playClickSound() async {
    final player = AudioPlayer();
    await player.play(
      AssetSource(
        "audio/sound.mp3",
      ),
      volume: 0.2,
    );
  }
}

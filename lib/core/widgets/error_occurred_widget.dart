import 'package:blist/app/widgets/blist_button.dart';
import 'package:blist/gen/assets.gen.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

class ErrorOccurredWidget extends StatelessWidget {
  const ErrorOccurredWidget({
    super.key,
    this.message,
    this.onRetry,
    required this.errorType,
  });

  final String? message;
  final ErrorType errorType;
  final VoidCallback? onRetry;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: switch (errorType) {
        ErrorType.message => textError(context),
        ErrorType.server => serverError(context),
        ErrorType.noInternet => noInternetError(context),
      },
    );
  }

  Widget textError(BuildContext context) => Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 16,
              vertical: 24,
            ),
            child: Text(
              message ?? '',
              textAlign: TextAlign.center,
              style: const TextStyle(fontSize: 24),
            ),
          ),
          if (onRetry != null)
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 36),
              child: BlistButton.filled(
                text: const Text('Try Again'),
                onPressed: onRetry,
              ),
            )
        ],
      );

  Widget serverError(BuildContext context) => Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const SizedBox(height: 24),
          // Get rid of the extra padding on the Lottie animation
          SizedBox(
            height: 240,
            child: OverflowBox(
              minHeight: 500,
              maxHeight: 500,
              child: Lottie.asset(
                Assets.json.error,
                fit: BoxFit.contain,
              ),
            ),
          ),
          Text(
            message ?? 'Server Error',
            textAlign: TextAlign.center,
            style: const TextStyle(fontSize: 22),
          ),
          const SizedBox(height: 30),
          if (onRetry != null)
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 36),
              child: BlistButton.filled(
                text: const Text('Try Again'),
                onPressed: onRetry,
              ),
            ),
        ],
      );

  Widget noInternetError(BuildContext context) => Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          onRetry != null
              ? InkWell(
                  onTap: onRetry,
                  child: Lottie.asset(
                    Assets.json.noInternet,
                    fit: BoxFit.contain,
                  ),
                )
              : Lottie.asset(
                  Assets.json.noInternet,
                  fit: BoxFit.contain,
                ),
          Text(
            message ?? 'No Internet',
            textAlign: TextAlign.center,
            style: const TextStyle(color: Colors.black87, fontSize: 22),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 60),
            child: BlistButton.filled(
              text: const Text('Try again'),
              onPressed: onRetry,
            ),
          )
        ],
      );
}

enum ErrorType {
  message,
  server,
  noInternet,
}

import 'package:flutter/material.dart';

class TwoLineSnackBar extends StatelessWidget {
  final String message;
  final String? actionLabel;
  final VoidCallback? onActionPressed;

  const TwoLineSnackBar({
    required this.message,
    this.actionLabel,
    this.onActionPressed,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          message,
          style: const TextStyle(color: Colors.white),
        ),
        if (actionLabel != null)
          Align(
            alignment: AlignmentDirectional.centerEnd,
            child: TextButton(
              onPressed: onActionPressed,
              child: Text(actionLabel!),
            ),
          )
      ],
    );
  }
}

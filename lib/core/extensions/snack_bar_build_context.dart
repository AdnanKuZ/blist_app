import 'package:blist/app/theme/styles/colors.dart';
import 'package:flutter/material.dart';

extension SnackBarBuildContextExtension on BuildContext {
  void removeSnackBar(
      {SnackBarClosedReason reason = SnackBarClosedReason.hide}) {
    ScaffoldMessenger.of(this).removeCurrentSnackBar(reason: reason);
  }

  void showSnackBar(SnackBar snackBar) {
    ScaffoldMessenger.of(this)
      ..hideCurrentSnackBar()
      ..showSnackBar(snackBar);
  }

  void showCopySnackBar() {
    showBasicSnackBar('Item copied', icon: Icons.copy_rounded);
  }

  void showSaveSuccessSnackBar() {
    showSuccessSnackBar('Saved');
  }

  void showFailSnackBar(
    String message, {
    Duration? duration,
    SnackBarAction? action,
  }) {
    showBasicSnackBar(
      message,
      icon: Icons.error_rounded,
      iconColor: Theme.of(this).colorScheme.error,
      duration: duration,
      action: action,
    );
  }

  void showSuccessSnackBar(
    String message, {
    Duration? duration,
    SnackBarAction? action,
  }) {
    showBasicSnackBar(
      message,
      icon: Icons.check_circle_rounded,
      iconColor: Colors.green,
      duration: duration,
      action: action,
    );
  }

  void showBasicSnackBar(
    String message, {
    IconData? icon,
    Color? iconColor,
    Duration? duration,
    SnackBarAction? action,
  }) {
    final content = icon == null
        ? Text(message)
        : Row(
            children: [
              Icon(icon, color: iconColor),
              const SizedBox(width: 8),
              Expanded(child: Text(message)),
            ],
          );

    showSnackBar(
      SnackBar(
        duration: duration ?? const Duration(milliseconds: 4000),
        content: content,
        backgroundColor: AppColors.onPrimary,
        action: action,
      ),
    );
  }
}

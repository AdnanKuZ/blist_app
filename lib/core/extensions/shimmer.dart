import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

extension ShimmerExtension on Widget {
  Widget shimmer() => Shimmer.fromColors(
        baseColor: ShimmerContainer.shimmercolor,
        highlightColor: const Color(0x00000000),
        child: this,
      );
}

class ShimmerContainer extends StatelessWidget {
  const ShimmerContainer({
    this.width = 60,
    this.height = 60,
    this.margin = const EdgeInsets.symmetric(horizontal: 10),
    this.padding,
    this.borderRadius,
    super.key,
  });

  final double width;
  final double height;
  final EdgeInsets margin;
  final EdgeInsets? padding;
  final BorderRadiusGeometry? borderRadius;

  static const shimmercolor = Color(0x4D000000);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      width: width,
      margin: margin,
      padding: padding,
      decoration: BoxDecoration(
        color: shimmercolor,
        borderRadius: borderRadius,
      ),
    );
  }
}

class TextShimmer extends StatelessWidget {
  const TextShimmer({
    this.width,
    this.height,
    super.key,
  });

  final double? width;
  final double? height;

  @override
  Widget build(BuildContext context) {
    return ShimmerContainer(
      height: height ?? 28,
      width: width ?? 150,
      borderRadius: BorderRadius.circular(30),
    );
  }
}

class ListViewShimmerWidget extends StatelessWidget {
  const ListViewShimmerWidget({
    super.key,
    this.height = 64,
    this.listHeight = 64,
    this.child,
    this.length,
    this.padding,
    this.scrollDirection = Axis.horizontal,
  });

  final double height;
  final double listHeight;
  final Widget? child;
  final int? length;
  final Axis scrollDirection;
  final EdgeInsetsGeometry? padding;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: listHeight,
      child: ListView.separated(
        shrinkWrap: true,
        padding: padding,
        physics: const NeverScrollableScrollPhysics(),
        scrollDirection: scrollDirection,
        itemBuilder: (context, index) =>
            child ??
            ShimmerContainer(
              height: height,
              width: double.infinity,
              borderRadius: BorderRadius.circular(17),
            ),
        separatorBuilder: (context, index) => scrollDirection == Axis.horizontal
            ? const SizedBox(width: 12)
            : const SizedBox(height: 12),
        itemCount: length ?? 9,
      ),
    );
  }
}

class GridViewShimmer extends StatelessWidget {
  const GridViewShimmer({
    this.padding,
    this.child,
    super.key,
  });

  final EdgeInsets? padding;
  final Widget? child;

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.sizeOf(context);

    return Padding(
      padding: padding ??
          const EdgeInsets.symmetric(
            horizontal: 18,
            vertical: 22,
          ),
      child: GridView.builder(
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisSpacing: 22,
          mainAxisSpacing: 24,
          childAspectRatio: (screenSize.width / 3) / (screenSize.height * 0.21),
          crossAxisCount: 2,
        ),
        itemBuilder: (context, index) =>
            child ??
            ShimmerContainer(
              width: double.infinity,
              borderRadius: BorderRadius.circular(12),
              margin: EdgeInsets.zero,
            ),
        itemCount: 6,
      ),
    );
  }
}

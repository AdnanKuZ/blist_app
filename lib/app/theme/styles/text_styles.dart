import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

abstract class TextStyles {
  static TextStyle archivoTextStyle({
    Color color = Colors.white,
    double fontSize = 13,
    double? height,
    FontWeight fontWeight = FontWeight.w600,
  }) {
    return GoogleFonts.archivo(
      height: height,
      color: color,
      fontSize: fontSize,
      fontWeight: fontWeight,
    );
  }


}

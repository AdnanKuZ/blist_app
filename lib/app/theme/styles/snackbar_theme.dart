import 'package:flutter/material.dart';

import 'colors.dart';

const snackBarTheme = SnackBarThemeData(
  behavior: SnackBarBehavior.floating,
  actionTextColor: AppColors.green,
);

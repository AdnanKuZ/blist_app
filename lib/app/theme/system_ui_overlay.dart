
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

SystemUiOverlayStyle systemUiOverlayStyle({
  Brightness statusBarBrightness = Brightness.light,
  Brightness systemNavigationBarIconBrightness = Brightness.light,
}) {
  const bool systemNavigationBarContrastEnforced = false;

  return SystemUiOverlayStyle(
    statusBarBrightness: statusBarBrightness,
    statusBarIconBrightness: statusBarBrightness,
    statusBarColor: Colors.transparent,
    systemNavigationBarColor: Colors.transparent,
    systemNavigationBarIconBrightness: systemNavigationBarIconBrightness,
    systemNavigationBarContrastEnforced: systemNavigationBarContrastEnforced,
  );
}

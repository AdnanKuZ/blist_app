import 'package:flutter/material.dart';
import 'package:blist/app/theme/styles/colors.dart';

class BlistButton extends StatelessWidget {
  final Widget? leading;
  final Widget text;
  final VoidCallback? onPressed;
  final Widget? trailing;
  final bool expandText;
  final ButtonStyle style;
  final Color borderColor;
  final Color buttonColor;
  final double radius;

  const BlistButton.filled({
    super.key,
    required this.text,
    required this.onPressed,
    this.leading,
    this.trailing,
    this.buttonColor = AppColors.blue,
    this.radius = 5,
    this.expandText = true,
  })  : style = ButtonStyle.filled,
        borderColor = Colors.transparent;

  const BlistButton.outlined({
    super.key,
    required this.text,
    required this.onPressed,
    this.borderColor = AppColors.primary,
    this.leading,
    this.trailing,
    this.radius = 5,
    this.expandText = true,
  })  : style = ButtonStyle.outlined,
        buttonColor = Colors.transparent;

  const BlistButton.text({
    super.key,
    required this.text,
    this.radius = 5,
    required this.onPressed,
    this.trailing = const Icon(
      Icons.arrow_forward_ios,
      size: 10,
    ),
  })  : style = ButtonStyle.text,
        borderColor = Colors.transparent,
        leading = null,
        expandText = false,
        buttonColor = Colors.transparent;

  @override
  Widget build(BuildContext context) {
    final isEnabled = onPressed != null;
    Color? foregroundColor;
    Color? backgroundColor;

    if (style == ButtonStyle.outlined) {
      // foregroundColor = borderColor;
      foregroundColor = Colors.white;
    }

    if (!isEnabled) {
    } else if (style == ButtonStyle.filled) {
      foregroundColor = Colors.white;
    }
    backgroundColor = buttonColor;

    return AnimatedContainer(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(radius)),
        color: backgroundColor,
        border: isEnabled && style == ButtonStyle.outlined
            ? Border.all(
                color: borderColor,
                width: 1,
              )
            : null,
      ),
      clipBehavior: Clip.hardEdge,
      duration: const Duration(milliseconds: 200),
      child: MaterialButton(
        onPressed: onPressed,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(radius)),
        ),
        elevation: 0,
        highlightElevation: 0,
        color: Colors.transparent,
        padding: style != ButtonStyle.text
            ? const EdgeInsets.symmetric(
                horizontal: 14,
                vertical: 14,
              )
            : EdgeInsets.zero,
        child: IconTheme(
          data: IconThemeData(
            color: !isEnabled ? Colors.grey : foregroundColor,
          ),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              if (style != ButtonStyle.text)
                SizedBox.square(
                  dimension: 24,
                  child: leading,
                ),
              const SizedBox(width: 10),
              Flexible(
                flex: expandText ? 1 : 0,
                child: Center(
                  child: DefaultTextStyle(
                    style: DefaultTextStyle.of(context).style.copyWith(
                          fontSize: style != ButtonStyle.text ? 17 : 14,
                          fontWeight: FontWeight.w400,
                          color: !isEnabled ? Colors.white : foregroundColor,
                          decoration: style == ButtonStyle.text
                              ? TextDecoration.underline
                              : null,
                        ),
                    child: text,
                  ),
                ),
              ),
              if (style != ButtonStyle.text) const SizedBox(width: 10),
              SizedBox.square(
                dimension: 24,
                child: trailing,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

enum ButtonStyle {
  filled,
  outlined,
  text,
}

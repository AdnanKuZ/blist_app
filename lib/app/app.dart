import 'dart:async';

import 'package:blist/app/routing/route_generator.dart';
import 'package:blist/app/routing/route_navigator.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:blist/app/injection/injection.dart';
import 'package:blist/app/theme/styles/colors.dart';
import 'package:blist/core/services/shared_preferences_service.dart';

import 'theme/light_theme.dart';
import 'theme/system_ui_overlay.dart';


class MyApp extends StatelessWidget {
  static Future<void> init() async {
    WidgetsFlutterBinding.ensureInitialized();

    configureDependencies();

    await SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);

    await SystemChrome.setEnabledSystemUIMode(SystemUiMode.edgeToEdge);
    SystemChrome.setSystemUIOverlayStyle(systemUiOverlayStyle());
  }


  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'BList',
      color: AppColors.primary,
      debugShowCheckedModeBanner: false,
      navigatorKey: navigatorKey,
      initialRoute: initialNavigation(),
      onGenerateRoute: RouteGenerator.generateRoute,
      theme: lightTheme(),
    );
  }
}

String initialNavigation() {
  final stroage = getIt<SharedPreferencesService>();
  if (stroage.getUser() != null) {
    return Routes.todos;
  }
  return Routes.welcome;
}

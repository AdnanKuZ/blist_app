import 'package:flutter/material.dart';

import '../../../../app/theme/styles/colors.dart';
import '../widgets/boarding_animations/blur_background.dart';
import '../widgets/boarding_animations/body_section.dart';
import '../widgets/boarding_animations/logo_section.dart';

class WelcomePage extends StatelessWidget {
  const WelcomePage({super.key});
  
  static const backgroundDuration = 8000;

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      backgroundColor: AppColors.darkBlue,
      body: Stack(
        children: [
          BlurBackground(),
          LogoSection(),
          Positioned(bottom: 10, child: BodySection()),
        ],
      ),
    );
  }
}

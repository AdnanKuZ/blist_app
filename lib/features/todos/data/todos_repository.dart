import 'package:blist/features/todos/data/models/todo_model.dart';
import 'package:blist/features/todos/data/todos_datasource.dart';
import 'package:injectable/injectable.dart';

import '../../../../../core/network/repository.dart';

@lazySingleton
class TodosRepository extends BaseRepository {
  TodosRepository(this._todosDataSource);

  final TodosDataSource _todosDataSource;

  Future addTodoRepository({
    required String todo,
    required bool isCompleted,
    required String userId,
  }) async =>
      await repository(
        () async => await _todosDataSource.addTodoDataSource(
          {
            'todo': todo,
            'completed': isCompleted,
            'userId': userId,
          },
        ),
      );
  Future<TodosModel> fetchTodosRepository({
    required Map<String, dynamic> queries,
  }) async =>
      await repository(
        () async => await _todosDataSource.fetchTodosDataSource(
          queries,
        ),
        modelParser: TodosModel.fromJson,
      );

  Future<TodoModel> fetchSingleTodoRepository({
    required String todoId,
  }) async =>
      await repository(
        () async => await _todosDataSource.fetchSingleTodoDataSource(
          todoId,
        ),
        modelParser: TodoModel.fromJson,
      );

  Future updateTodoRepository({
    required String todoId,
    required String todo,
    required bool isCompleted,
  }) async =>
      await repository(
        () async => await _todosDataSource.updateTodoDataSource(
          todoId,
          {
            'todo': todo,
            'completed': isCompleted,
          },
        ),
      );
  Future deleteTodoRepository({
    required String todoId,
  }) async =>
      await repository(
        () async => await _todosDataSource.deleteTodoDataSource(
          todoId,
        ),
      );
}

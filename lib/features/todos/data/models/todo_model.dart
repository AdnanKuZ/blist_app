import 'package:blist/core/network/models/pagination_model.dart';

class TodosModel {
  final PaginationModel paginationModel;
  final List<TodoModel> todos;

  TodosModel({
    required this.paginationModel,
    required this.todos,
  });

  TodosModel copyWith({
    PaginationModel? paginationModel,
    List<TodoModel>? todos,
  }) =>
      TodosModel(
        paginationModel: paginationModel ?? this.paginationModel,
        todos: todos ?? this.todos,
      );

  factory TodosModel.fromJson(Map<String, dynamic> json) => TodosModel(
        paginationModel: PaginationModel(
          total: json['total'],
          limit: json['limit'],
          skip: json['skip'],
        ),
        todos: List<TodoModel>.from(
          json['todos'].map(
            (x) => TodoModel.fromJson(x),
          ),
        ),
      );
}

class TodoModel {
  final int id;
  final String todo;
  final bool completed;
  final int userId;

  TodoModel({
    required this.id,
    required this.todo,
    this.completed = false,
    required this.userId,
  });

  TodoModel copyWith({
    int? id,
    String? todo,
    bool? completed,
    int? userId,
  }) =>
      TodoModel(
        id: id ?? this.id,
        todo: todo ?? this.todo,
        completed: completed ?? this.completed,
        userId: userId ?? this.userId,
      );

  factory TodoModel.fromJson(Map<String, dynamic> json) => TodoModel(
        id: json["id"],
        todo: json["todo"],
        completed: json["completed"],
        userId: json["userId"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "todo": todo,
        "completed": completed,
        "userId": userId,
      };
}

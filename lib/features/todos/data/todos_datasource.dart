import 'package:injectable/injectable.dart';

import '../../../../../core/network/datasource.dart';
import '../../../../../core/network/urls.dart';

@lazySingleton
class TodosDataSource extends BaseDataSource {
  TodosDataSource(super.dioService);

  Future<Map<String, dynamic>> addTodoDataSource(
    Map<String, dynamic> data,
  ) async =>
      await makeRequest(
        endpoint: ApiUrls.addtodosUrl,
        method: RequestMethod.post,
        data: data,
      );
  Future<Map<String, dynamic>> fetchTodosDataSource(
    Map<String, dynamic> queries,
  ) async =>
      await makeRequest(
        endpoint: ApiUrls.todosUrl,
        method: RequestMethod.get,
        queryParams: queries,
      );
  Future<Map<String, dynamic>> fetchSingleTodoDataSource(
    String todoId,
  ) async =>
      await makeRequest(
        endpoint: ApiUrls.todoUrl(todoId),
        method: RequestMethod.get,
      );
  Future<Map<String, dynamic>> updateTodoDataSource(
    String todoId,
    Map<String, dynamic> data,
  ) async =>
      await makeRequest(
        endpoint: ApiUrls.todoUrl(todoId),
        method: RequestMethod.put,
        data: data,
      );
  Future<Map<String, dynamic>> deleteTodoDataSource(
    String todoId,
  ) async =>
      await makeRequest(
        endpoint: ApiUrls.todoUrl(todoId),
        method: RequestMethod.delete,
      );
}

import 'package:blist/app/constants/app_constants.dart';
import 'package:blist/core/network/dio/exceptions.dart';
import 'package:blist/features/todos/data/models/todo_model.dart';
import 'package:blist/features/todos/data/todos_repository.dart';
import 'package:blist/features/todos/presentation/blocs/fetch_todos/fetch_todos_event.dart';
import 'package:blist/features/todos/presentation/blocs/fetch_todos/fetch_todos_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';

@injectable
class FetchTodosBloc extends Bloc<FetchTodosEvent, FetchTodosState> {
  final TodosRepository _todosRepository;
  FetchTodosBloc(this._todosRepository)
      : super(const InitialFetchTodosState()) {
    on<ClickFetchTodosEvent>(_onClickFetchTodosEvent);
    on<LoadMoreTodosEvent>(_onLoadMoreTodosEvent);
  }

  Future<void> _onClickFetchTodosEvent(
      ClickFetchTodosEvent event, Emitter<FetchTodosState> emit) async {
    try {
      emit(const LoadingState());
      final todos = await _todosRepository.fetchTodosRepository(queries: {
        "limit": AppConstants.todosFetchLimit,
      });
      emit(GotTodosSuccessState(todos));
    } on CustomException catch (ex) {
      emit(ErrorState(ex.message, ex.exceptionType));
    } catch (e) {
      emit(const ErrorState(
          'An unexpected error occurred', ExceptionType.unRecognized));
    }
  }

  Future<void> _onLoadMoreTodosEvent(
      LoadMoreTodosEvent event, Emitter<FetchTodosState> emit) async {
    try {
      final jsonResult = await _todosRepository.fetchTodosRepository(queries: {
        "limit": AppConstants.todosFetchLimit,
        // We can pass down the skip data instead of page count
        "skip": AppConstants.todosFetchLimit * (event.page - 1),
      });
      TodosModel finalModel = TodosModel(
        paginationModel: jsonResult.paginationModel,
        todos: [...event.oldTodos, ...jsonResult.todos],
      );
      emit(GotTodosSuccessState(finalModel));
    } on CustomException catch (ex) {
      emit(ErrorState(ex.message, ex.exceptionType));
    } catch (e) {
      emit(const ErrorState(
          'An unexpected error occurred', ExceptionType.unRecognized));
    }
  }
}

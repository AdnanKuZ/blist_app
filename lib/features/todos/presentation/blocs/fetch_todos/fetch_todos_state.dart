import 'package:blist/core/network/dio/exceptions.dart';
import 'package:blist/features/todos/data/models/todo_model.dart';

abstract class FetchTodosState {
  const FetchTodosState();
}

class InitialFetchTodosState extends FetchTodosState {
  const InitialFetchTodosState();
}

class LoadingState extends FetchTodosState {
  const LoadingState();
}

class GotTodosSuccessState extends FetchTodosState {
  final TodosModel todos;
  const GotTodosSuccessState(this.todos);
}

class ErrorState extends FetchTodosState {
  final String errorMessage;
  final ExceptionType exceptionType;
  const ErrorState(this.errorMessage, this.exceptionType);
}

class NoInternetState extends FetchTodosState {
  const NoInternetState();
}

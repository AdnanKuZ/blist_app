import 'package:blist/features/todos/data/models/todo_model.dart';

abstract class FetchTodosEvent {
  const FetchTodosEvent();
}

class ClickFetchTodosEvent extends FetchTodosEvent {
  ClickFetchTodosEvent();
}

class LoadMoreTodosEvent extends FetchTodosEvent {
  final int page;
  final List<TodoModel> oldTodos;
  LoadMoreTodosEvent(this.page,this.oldTodos);
}

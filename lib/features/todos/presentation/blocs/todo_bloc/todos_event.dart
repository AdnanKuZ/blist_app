abstract class TodosEvent {
  const TodosEvent();
}

class AddTodoEvent extends TodosEvent {
  final String todo;
  final bool isCompleted;
  final String userId;

  AddTodoEvent({
    required this.todo,
    required this.isCompleted,
    required this.userId,
  });
}

class UpdateTodosEvent extends TodosEvent {
  final String todoId;
  final String todo;
  final bool isCompleted;

  UpdateTodosEvent({
    required this.todoId,
    required this.todo,
    required this.isCompleted,
  });
}

class DeleteTodosEvent extends TodosEvent {
  final String todoId;

  DeleteTodosEvent({
    required this.todoId,
  });
}

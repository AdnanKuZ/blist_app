import 'package:blist/app/injection/injection.dart';
import 'package:blist/core/services/shared_preferences_service.dart';
import 'package:blist/features/todos/data/todos_repository.dart';
import 'package:blist/features/todos/presentation/blocs/todo_bloc/todos_event.dart';
import 'package:blist/features/todos/presentation/blocs/todo_bloc/todos_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';
import '../../../../../core/network/dio/exceptions.dart';

@injectable
class TodosBloc extends Bloc<TodosEvent, TodosState> {
  final TodosRepository _todosRepository;
  TodosBloc(this._todosRepository) : super(const InitialTodosState()) {
    on<AddTodoEvent>(_onAddTodoEvent);
    on<UpdateTodosEvent>(_onUpdateTodosEvent);
    on<DeleteTodosEvent>(_onDeleteTodosEvent);
  }

  Future<void> _onAddTodoEvent(
      AddTodoEvent event, Emitter<TodosState> emit) async {
    try {
      emit(const LoadingState());
      await _todosRepository.addTodoRepository(
        todo: event.todo,
        isCompleted: event.isCompleted,
        userId: event.userId,
      );
      emit(const TodosSuccessState());
    } on CustomException catch (ex) {
      emit(ErrorState(ex.message, ex.exceptionType));
    } catch (e) {
      emit(const ErrorState(
          'An unexpected error occurred', ExceptionType.unRecognized));
    }
  }

  Future<void> _onUpdateTodosEvent(
      UpdateTodosEvent event, Emitter<TodosState> emit) async {
    try {
      emit(SingleTodoLoadingState(event.todoId));
      await _todosRepository.updateTodoRepository(
        todoId: event.todoId,
        todo: event.todo,
        isCompleted: event.isCompleted,
      );
      emit(const TodosSuccessState());
    } on CustomException catch (ex) {
      emit(ErrorState(ex.message, ex.exceptionType));
    } catch (e) {
      emit(const ErrorState(
          'An unexpected error occurred', ExceptionType.unRecognized));
    }
  }

  Future<void> _onDeleteTodosEvent(
      DeleteTodosEvent event, Emitter<TodosState> emit) async {
    try {
      emit(const LoadingState());
      await _todosRepository.deleteTodoRepository(
        todoId: event.todoId,
      );
      emit(const TodosSuccessState());
    } on CustomException catch (ex) {
      emit(ErrorState(ex.message, ex.exceptionType));
    } catch (e) {
      emit(const ErrorState(
          'An unexpected error occurred', ExceptionType.unRecognized));
    }
  }
}

import 'package:blist/app/injection/injection.dart';
import 'package:blist/app/theme/styles/colors.dart';
import 'package:blist/core/services/shared_preferences_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:blist/features/todos/presentation/blocs/todo_bloc/todos_bloc.dart';
import 'package:blist/features/todos/presentation/blocs/todo_bloc/todos_event.dart';
import 'package:blist/features/todos/presentation/widgets/add_todo_dialog.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class AddTodoFloatingActionButton extends StatelessWidget {
  const AddTodoFloatingActionButton({super.key});

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      backgroundColor: AppColors.darkBlue,
      onPressed: () {
        showAddTodoDialog(context);
      },
      child: const Icon(
        CupertinoIcons.add,
        color: Colors.white,
        size: 28,
      ),
    );
  }

  void showAddTodoDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (builderContext) {
        return AddTodoDialog(
          onAdd: (String todo) {
            context.read<TodosBloc>().add(
                  AddTodoEvent(
                    todo: todo,
                    isCompleted: false,
                    userId: getIt<SharedPreferencesService>()
                        .getUser()!
                        .id
                        .toString(),
                  ),
                );
            Navigator.pop(context);
          },
        );
      },
    );
  }
}

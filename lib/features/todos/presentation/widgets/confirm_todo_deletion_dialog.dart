import 'package:blist/app/theme/styles/colors.dart';
import 'package:flutter/material.dart';

class DeleteTodoDialog extends StatelessWidget {
  const DeleteTodoDialog({super.key});

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      backgroundColor: AppColors.onPrimary,
      title: const Text(
        'Delete Confirmation',
        style: TextStyle(color: Colors.white),
      ),
      content: const Text('Are you sure you want to delete this todo?'),
      actions: <Widget>[
        TextButton(
          child: const Text('Cancel'),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        TextButton(
          child: const Text('Delete'),
          onPressed: () {
            // Add your delete logic here
            Navigator.pop(context, true);
          },
        ),
      ],
    );
  }
}

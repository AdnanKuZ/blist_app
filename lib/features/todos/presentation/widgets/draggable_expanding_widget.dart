import 'package:blist/features/todos/presentation/widgets/todo_tile.dart';
import 'package:flutter/material.dart';
import 'dart:async';

class DraggableExpandingWidget extends StatefulWidget {
  const DraggableExpandingWidget({
    required this.color,
    required this.deleteTodoCallback,
    super.key,
  });
  final Color color;
  final VoidCallback deleteTodoCallback;
  @override
  State<DraggableExpandingWidget> createState() =>
      _DraggableExpandingWidgetState();
}

class _DraggableExpandingWidgetState extends State<DraggableExpandingWidget>
    with SingleTickerProviderStateMixin {
  double _initialWidth = 20; // Constant initial width
  double _width = 20; // Changable width of the widget
  late double _maxWidth; // Maximum width of the drag

  late AnimationController _controller;
  late Animation<double> _animation;

  bool isCalled = false;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 50),
    );
    // Add a listener to update the width during the animation
    _controller.addListener(() {
      setState(() {
        _width = _animation.value;
      });
      if (_width > _initialWidth * 2 && !isCalled) {
        isCalled = true;
        widget.deleteTodoCallback();
      }
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  // Called when the user drags the widget
  void _onPanUpdate(DragUpdateDetails details) {
    setState(() {
      // Update the width based on the horizontal drag distance
      _width += details.delta.dx;
      // Ensure the width does not exceed the maximum or go below the initial width
      if (_width > _maxWidth) {
        _width = _maxWidth;
      } else if (_width < _initialWidth) {
        _width = _initialWidth;
      }
    });
  }

  // Called when the user stops dragging
  void _onPanEnd(DragEndDetails details) {
    // Reset the width back to the initial width
    isCalled = false;
    _animation =
        Tween<double>(begin: _width, end: _initialWidth).animate(_controller);
    _controller.forward(from: 0);
  }

  @override
  Widget build(BuildContext context) {
    // Maximum width
    _maxWidth = MediaQuery.sizeOf(context).width * .6;

    return GestureDetector(
      onPanUpdate: _onPanUpdate,
      onPanEnd: _onPanEnd,
      child: AnimatedContainer(
        duration: const Duration(milliseconds: 50),
        width: _width,
        height: 70,
        decoration: BoxDecoration(
          color: widget.color,
          borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(TodoTile.radius),
            bottomLeft: Radius.circular(TodoTile.radius),
          ),
        ),
      ),
    );
  }
}

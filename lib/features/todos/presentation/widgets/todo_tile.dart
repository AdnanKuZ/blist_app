import 'package:blist/app/theme/styles/colors.dart';
import 'package:blist/core/extensions/sizedbox_extension.dart';
import 'package:blist/core/extensions/snack_bar_build_context.dart';
import 'package:blist/core/mixins/pagination_mixin.dart';
import 'package:blist/core/services/audio_service.dart';
import 'package:blist/core/widgets/auto_scroll_text.dart';
import 'package:blist/core/widgets/custom_checkbox.dart';
import 'package:blist/features/todos/data/models/todo_model.dart';
import 'package:blist/features/todos/presentation/blocs/fetch_todos/fetch_todos_bloc.dart';
import 'package:blist/features/todos/presentation/blocs/fetch_todos/fetch_todos_event.dart';
import 'package:blist/features/todos/presentation/blocs/todo_bloc/todos_bloc.dart';
import 'package:blist/features/todos/presentation/blocs/todo_bloc/todos_event.dart';
import 'package:blist/features/todos/presentation/blocs/todo_bloc/todos_state.dart';
import 'package:blist/features/todos/presentation/widgets/confirm_todo_deletion_dialog.dart';
import 'package:blist/features/todos/presentation/widgets/draggable_expanding_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:vibration/vibration.dart';

class TodoTile extends StatefulWidget {
  const TodoTile({
    required this.todo,
    this.sideColor = AppColors.violet,
    super.key,
  });
  final TodoModel todo;
  final Color sideColor;

  static const double radius = 6;
  static const double tileHeight = 70;
  @override
  State<TodoTile> createState() => _TodoTileState();
}

class _TodoTileState extends State<TodoTile>
    with SingleTickerProviderStateMixin, PaginationMixin {
  late AnimationController _controller;
  late Animation<double> _animation;
  late Animation<double> _animation2;

  @override
  void initState() {
    super.initState();
    initAnimations();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.sizeOf(context).width;
    return Opacity(
      opacity: _animation.value,
      child: Transform.translate(
        offset: Offset(0, _animation2.value),
        child: InkWell(
          onTap: () {},
          splashColor: AppColors.textField,
          borderRadius: BorderRadius.circular(TodoTile.radius),
          child: Container(
            decoration: const BoxDecoration(color: AppColors.onPrimary),
            width: width,
            height: TodoTile.tileHeight,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                DraggableExpandingWidget(
                  color: widget.sideColor,
                  deleteTodoCallback: () async {
                    Vibration.vibrate(
                      duration: 50,
                      amplitude: 255,
                    );
                    showDeleteConfirmationDialog(context).then(
                      (value) {
                        if (value != null && value) {
                          context.read<TodosBloc>().add(
                                DeleteTodosEvent(
                                  todoId: widget.todo.id.toString(),
                                ),
                              );
                        }
                      },
                    );
                  },
                ),
                10.h(),
                Expanded(
                  child: AutoScrollText(
                    direction: Axis.vertical,
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        widget.todo.todo,
                        style: TextStyle(
                          fontWeight: FontWeight.w400,
                          fontSize: 14,
                          color: Colors.white.withOpacity(.9),
                        ),
                      ),
                    ),
                  ),
                ),
                16.h(),
                BlocConsumer<TodosBloc, TodosState>(
                  listener: _listener,
                  builder: (context, state) {
                    return MyCustomCheckbox(
                      value: widget.todo.completed,
                      borderColor: AppColors.violet,
                      loading: state is SingleTodoLoadingState &&
                          int.parse(state.todoId) == widget.todo.id,
                      onChanged: (value) {
                        AudioService.playClickSound();
                        context.read<TodosBloc>().add(
                              UpdateTodosEvent(
                                todoId: widget.todo.id.toString(),
                                todo: widget.todo.todo,
                                isCompleted: !widget.todo.completed,
                              ),
                            );
                      },
                    );
                  },
                ),
                10.h(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _listener(BuildContext context, state) {
    if (state is TodosSuccessState) {
      context.showSuccessSnackBar(
          'Task Successful\nNote that updating a todo will not update it in the server.');
      context.read<FetchTodosBloc>().add(ClickFetchTodosEvent());
    } else if (state is ErrorState) {
      context.showFailSnackBar('Fail');
    }
  }

  void initAnimations() {
    _controller = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 300),
    );

    _animation = Tween<double>(begin: 0, end: 1)
        .animate(CurvedAnimation(parent: _controller, curve: Curves.easeOut))
      ..addListener(() {
        setState(() {});
      });

    _animation2 = Tween<double>(begin: 30, end: 0)
        .animate(CurvedAnimation(parent: _controller, curve: Curves.easeOut));

    _controller.forward();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  Future<bool?> showDeleteConfirmationDialog(BuildContext context) {
    return showDialog<bool?>(
      context: context,
      builder: (BuildContext context) {
        return const DeleteTodoDialog();
      },
    );
  }
}

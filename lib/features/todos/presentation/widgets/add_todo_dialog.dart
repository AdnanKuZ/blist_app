import 'package:blist/app/injection/injection.dart';
import 'package:blist/app/theme/styles/colors.dart';
import 'package:blist/features/todos/presentation/blocs/todo_bloc/todos_bloc.dart';
import 'package:blist/features/todos/presentation/blocs/todo_bloc/todos_state.dart';
import 'package:blist/features/todos/presentation/widgets/todo_text_field.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class AddTodoDialog extends StatefulWidget {
  final Function(String) onAdd;

  const AddTodoDialog({super.key, required this.onAdd});

  @override
  State<AddTodoDialog> createState() => _AddTodoDialogState();
}

class _AddTodoDialogState extends State<AddTodoDialog> {
  late final TextEditingController _controller;
  late final GlobalKey<FormState> _formKey;
  @override
  void initState() {
    _controller = TextEditingController();
    _formKey = GlobalKey<FormState>();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<TodosBloc, TodosState>(
      bloc: getIt<TodosBloc>(),
      listener: (context, state) {
        if (state is LoadingState) {}
      },
      child: Stack(
        children: [
          AlertDialog(
            title: const Text(
              'Add Todo',
              style: TextStyle(color: Colors.white),
            ),
            backgroundColor: AppColors.onPrimary,
            content: Form(key: _formKey, child: TodoTextField(_controller)),
            actions: [
              TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: const Text('Cancel'),
              ),
              TextButton(
                onPressed: () {
                  if (_formKey.currentState!.validate()) {
                    widget.onAdd(_controller.text);
                  }
                },
                child: const Text('Add'),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

import 'package:blist/core/extensions/sizedbox_extension.dart';
import 'package:flutter/material.dart';

class TodosPageTopSection extends StatelessWidget {
  const TodosPageTopSection({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'To do Tasks',
          style: Theme.of(context).textTheme.titleLarge,
        ),
        20.v(),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            const Text(
              'Today\'s Tasks',
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.w600,
                  fontSize: 18),
            ),
            TextButton(
              child: const Text(
                "See All",
              ),
              onPressed: () {},
            ),
          ],
        )
      ],
    );
  }
}

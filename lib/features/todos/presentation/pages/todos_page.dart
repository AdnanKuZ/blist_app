import 'package:blist/app/injection/injection.dart';
import 'package:blist/app/theme/styles/colors.dart';
import 'package:blist/app/widgets/blist_appbar.dart';
import 'package:blist/app/widgets/blist_drawer.dart';
import 'package:blist/core/extensions/sizedbox_extension.dart';
import 'package:blist/core/mixins/pagination_mixin.dart';
import 'package:blist/core/services/shared_preferences_service.dart';
import 'package:blist/core/widgets/error_occurred_widget.dart';
import 'package:blist/core/widgets/loading.dart';
import 'package:blist/features/todos/data/models/todo_model.dart';
import 'package:blist/features/todos/presentation/blocs/fetch_todos/fetch_todos_bloc.dart';
import 'package:blist/features/todos/presentation/blocs/fetch_todos/fetch_todos_event.dart';
import 'package:blist/features/todos/presentation/blocs/fetch_todos/fetch_todos_state.dart';
import 'package:blist/features/todos/presentation/widgets/add_todo_floating_button.dart';
import 'package:blist/features/todos/presentation/widgets/todo_tile.dart';
import 'package:blist/features/todos/presentation/widgets/todos_top_section.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class TodosPage extends StatefulWidget {
  const TodosPage({super.key});

  @override
  State<TodosPage> createState() => _TodosPageState();
}

class _TodosPageState extends State<TodosPage> with PaginationMixin {
  TodosModel? todosModel;
  @override
  void initState() {
    super.initState();
    context.read<FetchTodosBloc>().add(ClickFetchTodosEvent());
  }

  @override
  Widget build(BuildContext context) {
    final storage = getIt<SharedPreferencesService>();

    return Scaffold(
      backgroundColor: AppColors.primary,
      drawer: const BlistDrawer(),
      floatingActionButton: const AddTodoFloatingActionButton(),
      appBar: BlistAppBar(
        isHome: true,
        title: Text(storage.getUser()?.username ?? 'BList'),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 18),
        child: BlocConsumer<FetchTodosBloc, FetchTodosState>(
          listener: _listener,
          builder: (context, state) {
            if (state is GotTodosSuccessState && todosModel != null) {
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  26.v(),
                  const TodosPageTopSection(),
                  10.v(),
                  Expanded(
                    child: RefreshIndicator(
                      onRefresh: () async => context
                          .read<FetchTodosBloc>()
                          .add(ClickFetchTodosEvent()),
                      child: ListView.separated(
                        controller: scrollController
                          ..addListener(_scrollListener),
                        itemCount: todosModel!.todos.length,
                        separatorBuilder: (context, index) => 10.v(),
                        itemBuilder: (context, index) => TodoTile(
                          todo: todosModel!.todos[index],
                          sideColor: AppColors.tileColors[index % 5],
                        ),
                      ),
                    ),
                  ),
                ],
              );
            } else if (state is LoadingState) {
              //A shimmer effect could be placed for better user experience
              return const Center(
                child: LoadingOverlay(),
              );
            }
            if (state is ErrorState) {
              return ErrorOccurredWidget(
                errorType: ErrorType.server,
                message: state.errorMessage,
                onRetry: () =>
                    context.read<FetchTodosBloc>().add(ClickFetchTodosEvent()),
              );
            }
            return const SizedBox();
          },
        ),
      ),
    );
  }

  //This listens to scroll changes and calls the pagination function on the scroll controller
  void _scrollListener() {
    //make sure to not paginate any more than the page count
    //make sure pagination isn't called when loadingMore is the current status
    if (pageCount != null &&
        page < pageCount! &&
        !isLoadingMore &&
        todosModel != null) {
      pagination(() {
        isLoadingMore = true;
        //call the load more todos bloc
        context
            .read<FetchTodosBloc>()
            .add(LoadMoreTodosEvent(page, todosModel!.todos));
      });
    }
  }

  void _listener(BuildContext context, FetchTodosState state) {
    if (state is GotTodosSuccessState) {
      todosModel = state.todos;
      pageCount = state.todos.paginationModel.pageCount;
      isLoadingMore = false;
    } else {
      //Reset values with error states
      isLoadingMore = false;
      page = 1;
    }
  }
}

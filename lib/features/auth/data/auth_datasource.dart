import 'package:blist/core/network/datasource.dart';
import 'package:blist/core/network/urls.dart';
import 'package:injectable/injectable.dart';

@lazySingleton
class AuthDataSource extends BaseDataSource {
  AuthDataSource(super.dioService);

  Future<Map<String, dynamic>> login(
    Map<String, dynamic> data,
  ) async =>
      //makeRequest returns a Future<Map<String, dynamic>>
      await makeRequest(
        endpoint: ApiUrls.loginUrl,
        method: RequestMethod.post,
        data: data,
      );
}

import 'package:flutter/material.dart';
import 'package:blist/app/theme/styles/colors.dart';

class TopLoginSection extends StatelessWidget {
  const TopLoginSection({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Log in',
          style: Theme.of(context).textTheme.titleLarge,
        ),
        Row(
          children: [
            const Flexible(
              child: Text(
                'Not a registered user?',
                style: TextStyle(color: AppColors.hint),
              ),
            ),
            TextButton(
              onPressed: () {},
              child: const Text(
                'Sign up',
                style: TextStyle(color: AppColors.lightGreen),
              ),
            )
          ],
        ),
      ],
    );
  }
}

import 'package:flutter/material.dart';
import 'package:blist/core/utils/validators.dart';

typedef FieldSubmit = Function(String?)?;

class NameField extends StatelessWidget {
  const NameField({
    required this.controller,
    this.node,
    this.onSubmit,
    super.key,
  });
  final TextEditingController controller;
  final FocusNode? node;
  final FieldSubmit onSubmit;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      onFieldSubmitted: onSubmit,
      focusNode: node,
      decoration: const InputDecoration(hintText: 'Full Name'),
      autovalidateMode: AutovalidateMode.onUserInteraction,
      validator: Validators.nameValidator,
    );
  }
}

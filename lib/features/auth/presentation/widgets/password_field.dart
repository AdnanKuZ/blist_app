import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:blist/core/utils/validators.dart';
import 'package:blist/features/auth/presentation/widgets/name_field.dart';

import '../../../../app/widgets/toggle_password_icon_button.dart';

class PasswordField extends StatelessWidget {
  const PasswordField({
    required this.controller,
    required this.isObscur,
    required this.obscureText,
    this.node,
    this.onSubmit,
    super.key,
  });
  final TextEditingController controller;
  final bool isObscur;
  final VoidCallback obscureText;
  final FocusNode? node;
  final FieldSubmit onSubmit;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      focusNode: node,
      onFieldSubmitted: onSubmit,
      decoration: InputDecoration(
        hintText: 'Password',
        suffixIcon: TogglePasswordIconButton(
          onPressed: obscureText,
          isObscured: isObscur,
        ),
      ),
      inputFormatters: [
        FilteringTextInputFormatter.deny(' '),
      ],
      autovalidateMode: AutovalidateMode.onUserInteraction,
      obscureText: isObscur,
      validator: Validators.passwordValidator,
    );
  }
}

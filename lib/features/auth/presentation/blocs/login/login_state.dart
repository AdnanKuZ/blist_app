
import 'package:blist/core/network/dio/exceptions.dart';
import 'package:blist/features/auth/data/models/user_model.dart';

abstract class LoginState {
  const LoginState();
}

class InitialLoginState extends LoginState {
  const InitialLoginState();
}

class LoadingState extends LoginState {
  const LoadingState();
}

class LoginSuccessState extends LoginState {
  final UserModel user;
  const LoginSuccessState(this.user);
}

class NotVerifiedState extends LoginState {
  const NotVerifiedState();
}



class ErrorState extends LoginState {
  final String errorMessage;
  final ExceptionType exceptionType;
  const ErrorState(this.errorMessage, this.exceptionType);
}

class NoInternetState extends LoginState {
  const NoInternetState();
}
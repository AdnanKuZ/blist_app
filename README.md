# BList

BList is a simple todo app that allows the user to add, update and delete todos.

## Table of Contents

- [Features list](#features-list)
- [First Run](#first-run)
- [Data Management](#data-management)
  - [DataSource Layer](#datasource-layer)
    - [ApiInterceptor class](#apiinterceptor-class)
    - [CustomException class](#customexception-class)
    - [DioService](#dioservice)
  - [Repository Layer](#repository-layer)
  - [Presentation Layer](#presentation-layer)
- [Dependency Management](#dependency-management)
- [Project Structure](#project-structure)

# Features list

- Authentication login
- Todos Creation
- Todos Edit
- Todos Deletion
- Todos Browsing
- Todos Pagination

The app is built with Clean Architecture and it's configured with [BLoC] for state management, [Dio] for networking, [GetIt] for Dependency Injetion

# First Run

Navigate to the project directory:

```
cd project
```

Install dependencies:

```
flutter pub get
```

Run the app

```
flutter run
```

# Data Management

The app handles data on three layers:

- DataSource Layer
- Repository Layer
- Presentation Layer

## DataSource Layer

First layer of processing data coming from the api.
Responsible for making network http requests using dio.
It handles exceptions during the network request and allows them to propagate up to the caller (e.g., the repository or bloc class) for further handling.
Returns the resulting json to the repository layer for further processing

The DataSource Layer collaborates with other helper classes, which function as additional abstraction layers for fetching the data from the api:

### [ApiInterceptor] class

An interceptor class for dio that handles intercepting requests and responses.
It injects the authorization token into the request headers and validates the success of the response.
If the response indicates failure, it throws a DioException to trigger error handling logic downstream.

### [CustomException] class

This class Provides utility methods to convert exceptions into custom exceptions with specific error codes and messages.

### [DioService]

The DioService class facilitates network requests using the [Dio] library, allowing customization of request parameters, cancellation, and integration with caching through the dio_cache_interceptor package.

It predefines the common http methods : GET, POST, DELETE, Update. To easily use in each datasource method

[CustomException]: lib/core/network/dio/exceptions.dart
[ApiInterceptor]: lib/core/network/dio/interceptors/api_interceptor.dart
[DioService]: lib/core/network/dio/dio_service.dart
[Dio]: https://pub.dev/packages/dio

## Repository Layer

Acts as an intermediary between the data source and higher-level components (e.g., the bloc class).
It calls the appropriate methods on the DataSource class and handles any exceptions thrown by the data source,
converting them into custom exceptions using the CustomErrors class.
Also parses the network response to the appropriate dart classes.

## Presentation Layer

The highest layer in the architecture, This layer manages the state of the app and is responsible for handling user interface (UI) interactions and displaying data to the user.
It is designed to be independent of the application's business logic and infrastructure details.

The project uses BLoc state managment and each feature's presentation layer is divided into:

- feature
  - data
  - presentation
    - pages
    - widgets
    - bloc
      - bloc.dart: Contains the implementation of the Bloc class, managing the application's business logic by processing events and emitting states.
      - bloc_state.dart: Defines state classes representing different UI snapshots, each corresponding to a specific screen or representation based on the business logic.
      - bloc_event.dart:Defines event classes representing triggers that initiate changes in the business logic, leading to the emission of new states.

# Dependency Management

Dependencies are auto generated in the [`injection_config.dart`] file. It uses the [GetIt] package, and uses the injectable package which is responsible for the generation depending on annotations used a lightweight service locator. These files are responsible for registering and initializing various dependencies, services, and blocs used throughout the application.

[di]: lib/app/di.dart
[GetIt]: https://pub.dev/packages/get_it

## SDK Version

This Flutter project requires a minimum SDK version of Flutter 3.22.0. Ensure that you have the appropriate Flutter SDK installed before running the application. You can download Flutter from [here](https://flutter.dev/docs/get-started/install).

```yaml
environment:
  sdk: '>=3.4.0 <4.0.0'
```

## External Dependencies

Here is a list of external dependencies used in this project:

```yaml
dependencies:
  flutter:
    sdk: flutter

  cupertino_icons: ^1.0.2
  
  ##State, Storage and DI 
  flutter_bloc: ^8.1.5
  get_it: ^7.2.0
  injectable: ^2.3.3
  shared_preferences: ^2.2.3


  ##Network
  async: ^2.11.0
  http: ^1.2.1
  dio: ^5.4.3+1
  dio_cache_interceptor: ^3.5.0

  ##Utils
  google_fonts: ^6.2.1
  intl: any

  ##UI
  lottie: ^3.1.0
  shimmer: ^3.0.0
  fluttertoast: ^8.2.5
  font_awesome_flutter: ^10.7.0
  cached_network_image: ^3.3.1
  photo_view: ^0.15.0
  flutter_svg: ^2.0.10+1
  flutter_native_splash: ^2.4.0
  audioplayers: ^6.0.0
  vibration: ^1.9.0

dev_dependencies:
  flutter_test:
    sdk: flutter

  flutter_lints: ^3.0.0
  #injection generator
  injectable_generator: ^2.4.0

  flutter_launcher_icons: ^0.13.1
  build_runner: ^2.4.9
  flutter_gen_runner: ^5.4.0
  mockito: ^5.0.17
  bloc_test: ^9.0.3
```

# Project Structure

This section showcases the project's files and folder and overall architecture

```
📦lib
 ┣ 📂app
 ┃ ┣ 📂constants
 ┃ ┃ ┣ 📜app_constants.dart
 ┃ ┃ ┣ 📜box_shadow.dart
 ┃ ┃ ┗ 📜date_time_constants.dart
 ┃ ┣ 📂injection
 ┃ ┃ ┣ 📂injection_modules.dart
 ┃ ┃ ┃ ┗ 📜dio.dart
 ┃ ┃ ┣ 📜injection.config.dart
 ┃ ┃ ┗ 📜injection.dart
 ┃ ┣ 📂routing
 ┃ ┃ ┣ 📜route_generator.dart
 ┃ ┃ ┣ 📜route_navigator.dart
 ┃ ┃ ┗ 📜routes.dart
 ┃ ┣ 📂theme
 ┃ ┃ ┣ 📂styles
 ┃ ┃ ┃ ┣ 📜app_bar_theme.dart
 ┃ ┃ ┃ ┣ 📜colors.dart
 ┃ ┃ ┃ ┣ 📜dialog_theme.dart
 ┃ ┃ ┃ ┣ 📜input_decoration_style.dart
 ┃ ┃ ┃ ┣ 📜snackbar_theme.dart
 ┃ ┃ ┃ ┗ 📜text_styles.dart
 ┃ ┃ ┣ 📜light_theme.dart
 ┃ ┃ ┣ 📜system_ui_overlay.dart
 ┃ ┃ ┗ 📜theme_manager.dart
 ┃ ┣ 📂widgets
 ┃ ┃ ┣ 📜blist_appbar.dart
 ┃ ┃ ┣ 📜blist_button.dart
 ┃ ┃ ┣ 📜blist_drawer.dart
 ┃ ┃ ┗ 📜toggle_password_icon_button.dart
 ┃ ┗ 📜app.dart
 ┣ 📂core
 ┃ ┣ 📂extensions
 ┃ ┃ ┣ 📜dialog_build_context.dart
 ┃ ┃ ┣ 📜shimmer.dart
 ┃ ┃ ┣ 📜sizedbox_extension.dart
 ┃ ┃ ┗ 📜snack_bar_build_context.dart
 ┃ ┣ 📂mixins
 ┃ ┃ ┗ 📜pagination_mixin.dart
 ┃ ┣ 📂network
 ┃ ┃ ┣ 📂dio
 ┃ ┃ ┃ ┣ 📂interceptors
 ┃ ┃ ┃ ┃ ┣ 📜api_interceptor.dart
 ┃ ┃ ┃ ┃ ┗ 📜logging_interceptor.dart
 ┃ ┃ ┃ ┣ 📜dio_service.dart
 ┃ ┃ ┃ ┗ 📜exceptions.dart
 ┃ ┃ ┣ 📂models
 ┃ ┃ ┃ ┗ 📜pagination_model.dart
 ┃ ┃ ┣ 📜datasource.dart
 ┃ ┃ ┣ 📜repository.dart
 ┃ ┃ ┗ 📜urls.dart
 ┃ ┣ 📂services
 ┃ ┃ ┣ 📜audio_service.dart
 ┃ ┃ ┗ 📜shared_preferences_service.dart
 ┃ ┣ 📂utils
 ┃ ┃ ┗ 📜validators.dart
 ┃ ┗ 📂widgets
 ┃ ┃ ┣ 📜auto_scroll_text.dart
 ┃ ┃ ┣ 📜custom_checkbox.dart
 ┃ ┃ ┣ 📜error_occurred_widget.dart
 ┃ ┃ ┣ 📜expandable_scrollable_column.dart
 ┃ ┃ ┣ 📜loading.dart
 ┃ ┃ ┣ 📜text_form_field.dart
 ┃ ┃ ┣ 📜two_line_snackbar.dart
 ┃ ┃ ┗ 📜under_development_page.dart
 ┣ 📂features
 ┃ ┣ 📂auth
 ┃ ┃ ┣ 📂data
 ┃ ┃ ┃ ┣ 📂models
 ┃ ┃ ┃ ┃ ┗ 📜user_model.dart
 ┃ ┃ ┃ ┣ 📜auth_datasource.dart
 ┃ ┃ ┃ ┗ 📜auth_repository.dart
 ┃ ┃ ┗ 📂presentation
 ┃ ┃ ┃ ┣ 📂blocs
 ┃ ┃ ┃ ┃ ┗ 📂login
 ┃ ┃ ┃ ┃ ┃ ┣ 📜login_bloc.dart
 ┃ ┃ ┃ ┃ ┃ ┣ 📜login_event.dart
 ┃ ┃ ┃ ┃ ┃ ┗ 📜login_state.dart
 ┃ ┃ ┃ ┣ 📂pages
 ┃ ┃ ┃ ┃ ┗ 📜login_page.dart
 ┃ ┃ ┃ ┣ 📂services
 ┃ ┃ ┃ ┃ ┗ 📜login_service.dart
 ┃ ┃ ┃ ┗ 📂widgets
 ┃ ┃ ┃ ┃ ┣ 📂login_page
 ┃ ┃ ┃ ┃ ┃ ┗ 📜top_login_section.dart
 ┃ ┃ ┃ ┃ ┣ 📜confirm_password_field.dart
 ┃ ┃ ┃ ┃ ┣ 📜name_field.dart
 ┃ ┃ ┃ ┃ ┗ 📜password_field.dart
 ┃ ┣ 📂todos
 ┃ ┃ ┣ 📂data
 ┃ ┃ ┃ ┣ 📂models
 ┃ ┃ ┃ ┃ ┗ 📜todo_model.dart
 ┃ ┃ ┃ ┣ 📜todos_datasource.dart
 ┃ ┃ ┃ ┗ 📜todos_repository.dart
 ┃ ┃ ┗ 📂presentation
 ┃ ┃ ┃ ┣ 📂blocs
 ┃ ┃ ┃ ┃ ┣ 📂fetch_todos
 ┃ ┃ ┃ ┃ ┃ ┣ 📜fetch_todos_bloc.dart
 ┃ ┃ ┃ ┃ ┃ ┣ 📜fetch_todos_event.dart
 ┃ ┃ ┃ ┃ ┃ ┗ 📜fetch_todos_state.dart
 ┃ ┃ ┃ ┃ ┗ 📂todo_bloc
 ┃ ┃ ┃ ┃ ┃ ┣ 📜todos_bloc.dart
 ┃ ┃ ┃ ┃ ┃ ┣ 📜todos_event.dart
 ┃ ┃ ┃ ┃ ┃ ┗ 📜todos_state.dart
 ┃ ┃ ┃ ┣ 📂pages
 ┃ ┃ ┃ ┃ ┗ 📜todos_page.dart
 ┃ ┃ ┃ ┗ 📂widgets
 ┃ ┃ ┃ ┃ ┣ 📜add_todo_dialog.dart
 ┃ ┃ ┃ ┃ ┣ 📜add_todo_floating_button.dart
 ┃ ┃ ┃ ┃ ┣ 📜confirm_todo_deletion_dialog.dart
 ┃ ┃ ┃ ┃ ┣ 📜draggable_expanding_widget.dart
 ┃ ┃ ┃ ┃ ┣ 📜todo_text_field.dart
 ┃ ┃ ┃ ┃ ┣ 📜todo_tile.dart
 ┃ ┃ ┃ ┃ ┗ 📜todos_top_section.dart
 ┃ ┗ 📂welcome
 ┃ ┃ ┗ 📂presentation
 ┃ ┃ ┃ ┣ 📂pages
 ┃ ┃ ┃ ┃ ┗ 📜welcome_page.dart
 ┃ ┃ ┃ ┗ 📂widgets
 ┃ ┃ ┃ ┃ ┗ 📂boarding_animations
 ┃ ┃ ┃ ┃ ┃ ┣ 📜blur_background.dart
 ┃ ┃ ┃ ┃ ┃ ┣ 📜body_section.dart
 ┃ ┃ ┃ ┃ ┃ ┗ 📜logo_section.dart
 ┣ 📂gen
 ┃ ┗ 📜assets.gen.dart
 ┗ 📜main.dart
```

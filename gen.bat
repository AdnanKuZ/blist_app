#!/bin/bash

# Check if a feature name is provided
if [ -z "$1" ]; then
    echo "Usage: $0 feature_name"
    exit 1
fi

# Run the Python script with the provided feature name
python3 feature_folder_gen_script.py "$1"

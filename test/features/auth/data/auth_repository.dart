import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:blist/features/auth/data/auth_repository.dart';
import 'package:blist/features/auth/data/auth_datasource.dart';
import 'package:blist/features/auth/data/models/user_model.dart';

// Mock class for AuthDataSource
class MockAuthDataSource extends Mock implements AuthDataSource {}

void main() {
  late AuthRepository authRepository;
  late MockAuthDataSource mockAuthDataSource;

  // Initialize the mock AuthDataSource and AuthRepository before each test
  setUp(() {
    mockAuthDataSource = MockAuthDataSource();
    authRepository = AuthRepository(mockAuthDataSource);
  });

  test('should return UserModel when loginRepository is called', () async {
    // Arrange: Define the behavior of the mock AuthDataSource
    final Map<String, dynamic> mockResponse = {
      "id": 1,
      "username": "atuny0",
      "email": "atuny0@sohu.com",
      "firstName": "Terry",
      "lastName": "Medhurst",
      "gender": "male",
      "image": "https://robohash.org/Terry.png?set=set4",
      "token": "String"
    };
    final data = {
      'username': 'atuny0',
      'password': '9uQFF1Lh',
    };
    when(
      authRepository.loginRepository(name: 'atuny0', password: '9uQFF1Lh'),
    ).thenAnswer(
      (_) async => UserModel(
        id: 1,
        username: 'atuny0',
        email: 'atuny0@sohu.com',
        firstName: 'Terry',
        lastName: 'Medhurst',
        gender: 'male',
        token: 'String',
      ),
    );

    // Act: Call the loginRepository method
    final UserModel result = await authRepository.loginRepository(
      name: 'atuny0',
      password: '9uQFF1Lh',
    );

    // Assert: Verify the result and that the method was called once
    expect(result, isA<UserModel>());
    verify(mockAuthDataSource.login(data)).called(1);
  });
}
